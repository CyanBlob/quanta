
/*
   An example digital clock using a TFT LCD screen to show the time.
   Demonstrates use of the font printing routines. (Time updates but date does not.)

   For a more accurate clock, it would be better to use the RTClib library.
   But this is just a demo. 

   This examples uses the hardware SPI only. Non-hardware SPI
   is just too slow (~8 times slower!)

   Based on clock sketch by Gilchrist 6/2/2014 1.0
   Updated by Bodmer
   A few colour codes:

   code	color
   0x0000	Black
   0xFFFF	White
   0xBDF7	Light Gray
   0x7BEF	Dark Gray
   0xF800	Red
   0xFFE0	Yellow
   0xFBE0	Orange
   0x79E0	Brown
   0x7E0	Green
   0x7FF	Cyan
   0x1F	Blue
   0xF81F	Pink
 */

#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <SPI.h>

#define IN_X_POS A0
#define IN_X_NEG 0
#define IN_Y_POS 0
#define IN_Y_NEG 0

#define READ_X D4
#define READ_Y D6

TFT_eSPI tft = TFT_eSPI();  // Invoke library, pins defined in User_Setup.h

struct Character {
    uint16_t x;
    uint16_t y;
    uint16_t radius;
    uint16_t color;
};

#define NUM_BADDIES 10
Character baddies[NUM_BADDIES];
Character player;

uint8_t frame = 0;
uint16_t lives = 0 - 1;

void preloop_update_frequency() {
#if defined(F_CPU) && (F_CPU != 160000000L)
    REG_SET_BIT(0x3ff00014, BIT(0));
    ets_update_cpu_frequency(160);
#endif
}

void setup(void) {
    Serial.begin(115200);
    pinMode(IN_X_POS, INPUT);
    pinMode(READ_X, OUTPUT);
    pinMode(READ_Y, OUTPUT);
    tft.init();
    tft.setRotation(0);
    tft.fillScreen(TFT_BLACK);

    tft.setTextColor(TFT_WHITE, TFT_BLACK); // Note: the new fonts do not draw the background colour

    tft.setCursor(0, tft.height());
    tft.print(lives);

    player = {(uint16_t)tft.width() / 2, (uint16_t)tft.height() / 2, 3, TFT_CYAN};

    baddies[0] = {0, 0, 5, TFT_GREEN};
    baddies[1] = {25, 25, 5, TFT_MAGENTA};
    baddies[2] = {50, 50, 5, TFT_YELLOW};
    baddies[3] = {75, 75, 5, TFT_RED};
    baddies[4] = {100, 100, 5, TFT_BLUE};
    baddies[5] = {25, 0, 5, TFT_GREEN};
    baddies[6] = {50, 25, 5, TFT_MAGENTA};
    baddies[7] = {75, 50, 5, TFT_YELLOW};
    baddies[8] = {100, 75, 5, TFT_RED};
    baddies[9] = {125, 100, 5, TFT_BLUE};
}

void loop()
{
    movePlayer();
    moveBaddies();
    if (detectCollisions())
    {
        --lives;
        Serial.print("Lives: ");
        Serial.println(lives);
    }
    printLives();
}

void printLives()
{
    tft.setCursor(0, tft.height() - 10);
    tft.print(lives);
}

bool detectCollisions()
{
    for (int x = 0; x < NUM_BADDIES; ++x)
    {
        //compare the distance to combined radii
        int dx = baddies[x].x - player.x;
        int dy = baddies[x].y - player.y;
        int radii = baddies[x].radius + player.radius;
        if ((dx * dx) + (dy * dy) < radii * radii) 
        {
            return true;
        }
    }
    return false;
}

void movePlayer()
{
    tft.fillCircle(player.x, player.y, player.radius,
            TFT_BLACK);

    digitalWrite(READ_X, HIGH);
    digitalWrite(READ_Y, LOW);
    int16_t x = analogRead(IN_X_POS);
    player.x = map(x, 0, 1024, 0, tft.width());

    digitalWrite(READ_X, LOW);
    digitalWrite(READ_Y, HIGH);
    int16_t y = analogRead(IN_X_POS);
    Serial.print("x: ");
    Serial.print(x);
    Serial.print(", y: ");
    Serial.println(y);
    player.y = map(y, 0, 1024, 0, tft.height());

    tft.fillCircle(player.x, player.y, player.radius,
            player.color);
}

void moveBaddies()
{
    for (int x = 0; x < NUM_BADDIES; ++x)
    {
        uint16_t prevX = baddies[x].x - 1;
        uint16_t prevY = baddies[x].y - 1;
        if (prevX > tft.width())
            prevX = tft.width();
        if (prevY > tft.height())
            prevY = tft.height();

        tft.fillCircle(prevX, prevY, baddies[x].radius,
                TFT_BLACK);

        baddies[x].x++;
        baddies[x].y++;

        if (baddies[x].x > tft.width())
            baddies[x].x = 0;
        if (baddies[x].y > tft.height())
            baddies[x].y = 0;

        // draw every other frame
        tft.fillCircle(baddies[x].x, baddies[x].y, baddies[x].radius,
                baddies[x].color);
        ESP.wdtFeed();
    }
    ++frame;
}
